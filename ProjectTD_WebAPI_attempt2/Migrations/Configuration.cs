using ProjectTD_WebAPI_attempt2.Models;
namespace ProjectTD_WebAPI_attempt2.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProjectTD_WebAPI_attempt2.Models.ProjectTD_WebAPI_attempt2Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProjectTD_WebAPI_attempt2.Models.ProjectTD_WebAPI_attempt2Context context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //okay, adding some default data
            //adding warehouses
            context.Warehouses.AddOrUpdate(x => x.Id,

                new Warehouse() { Id = 1, Warehouse_Name = "Mysore" },
                new Warehouse() { Id = 2, Warehouse_Name = "Bangalore" },
                new Warehouse() { Id = 3, Warehouse_Name = "Nanjangud" }
            );

            //adding headphones

            context.Headphones.AddOrUpdate(x => x.Id,

                new Headphone() { Id = 1, Name = "Skull Candy", Category = "Wireless", Price = 4000.10M, WarehouseId = 1 },
                new Headphone() { Id = 1, Name = "Sony", Category = "Wired", Price = 400.10M, WarehouseId = 2 },
                new Headphone() { Id = 1, Name = "JBL", Category = "Wireless", Price = 1000.10M, WarehouseId = 3 }
                );
        }
    }
}
