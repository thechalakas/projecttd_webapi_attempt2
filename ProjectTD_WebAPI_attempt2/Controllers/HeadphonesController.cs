﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ProjectTD_WebAPI_attempt2.Models;
using Newtonsoft.Json;

namespace ProjectTD_WebAPI_attempt2.Controllers
{
    public class HeadphonesController : ApiController
    {
        private ProjectTD_WebAPI_attempt2Context db = new ProjectTD_WebAPI_attempt2Context();

        // GET: api/Headphones
        //this one returns the entire set of headphones.
        //if you look at the commit before this line was written, you will notice that this method was returning the
        //headphone objects directly. I am replacing the direct objects with DTO objects that I have added along with the current
        //commit
        //If you dont know how commits work, you should learn. If not, none of these comments and code will ever make sense.
        //However, I know that, deep within your mind, you are not a moron, so you will learn how commits work :)

        //okay, lets do the replacing.
        public IQueryable<HeadphoneDTO> GetHeadphones()
        {
            //return db.Headphones; //this is the old pre-DTO code. 

            //running a LINQ query that will check the Headphones table
            //then, map the returned data to our DTO object
            //then, put it into the headphone_set collection

            var headphone_set = from h in db.Headphones
                                select new HeadphoneDTO()
                                {
                                    Id = h.Id,
                                    Name = h.Name,
                                    WarehouseName = h.Warehouse.Warehouse_Name
                                };

            return headphone_set;
        }

        ////alright, the above method GetHeadphones returns all headphones in XML format. 
        ////The following method will do the same but instead of XML, it will return the data in JSON format
        //// GET: api/HeadphonesJSON
        //[Route("api/HeadphonesJSON")]  //I am using attribute routing because this method and the GetHeadphones method are both mapping to 
        ////the same routing in the route map in RouteConfig
        //public string GetHeadphonesJSON()
        //{
        //    var headphone_set = from h in db.Headphones
        //                        select new HeadphoneDTO()
        //                        {
        //                            Id = h.Id,
        //                            Name = h.Name,
        //                            WarehouseName = h.Warehouse.Warehouse_Name
        //                        };

        //    //now need to convert the entire collection headphone_set into JSON
        //    var headphone_set_json = JsonConvert.SerializeObject(headphone_set);
        //    return headphone_set_json;
        //}


        //just as it is with GetHeadphones, modifying the below method also for DTO objects
        // GET: api/Headphones/5
        [ResponseType(typeof(HeadphoneDetailDTO))]
        public async Task<IHttpActionResult> GetHeadphone(int id)
        {

            /*
             * this is the old non DTO code
            Headphone headphone = await db.Headphones.FindAsync(id);
            if (headphone == null)
            {
                return NotFound();
            }

            return Ok(headphone);
            */

            //here, the select will select one result based on the criteria where the id being passed as parameter
            //matches the id of some row in the table
            //another important consideration is the Include part. We also need some data from the Warehouse table which is part 
            //of the headphone class. If not included, we wont be able to assign a value to the WarehouseName string. 
            var headphone_set = await db.Headphones.Include(h => h.Warehouse).Select(h =>
               new HeadphoneDetailDTO()
                   {
                       Id = h.Id,
                       Name = h.Name,
                       Category = h.Category,
                       Price = h.Price,
                       WarehouseName = h.Warehouse.Warehouse_Name
                   }
                ).SingleOrDefaultAsync(h => h.Id == id);

            if(headphone_set == null)
            {
                return NotFound();
            }

            return Ok(headphone_set);
        }

        ////just as it is with GetHeadphonesJSON, I need a JSON version of GetHeadphone(int id)
        //[Route("api/HeadphonesJSON/{id}")]
        //public async Task<IHttpActionResult> GetHeadphoneJSON(int id)
        //{
        //    var headphone_set = await db.Headphones.Include(h => h.Warehouse).Select(h =>
        //       new HeadphoneDetailDTO()
        //       {
        //           Id = h.Id,
        //           Name = h.Name,
        //           Category = h.Category,
        //           Price = h.Price,
        //           WarehouseName = h.Warehouse.Warehouse_Name
        //       }
        //        ).SingleOrDefaultAsync(h => h.Id == id);

        //    if (headphone_set == null)
        //    {
        //        return NotFound();
        //    }

        //    var headphone_set_json = JsonConvert.SerializeObject(headphone_set); //converting the headphone set to JSON string
        //    return Ok(headphone_set_json);
        //}

        // PUT: api/Headphones/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutHeadphone(int id, Headphone headphone)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != headphone.Id)
            {
                return BadRequest();
            }

            db.Entry(headphone).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HeadphoneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        
        // POST: api/Headphones
        //this method will add a new headphone row to the table.
        //I will have to modify it to work with a DTO, just as I have done above. 
        [ResponseType(typeof(Headphone))]
        public async Task<IHttpActionResult> PostHeadphone(Headphone headphone)
        {
            /*
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Headphones.Add(headphone);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = headphone.Id }, headphone);*/

            //this checks if the headphone object that is sent is fully loaded with the essential stuff
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //I need to get the headphone details and add it to the headphone object.
            //var item1 = Items.FirstOrDefault(x => x.Id == 123);
            var warehouse_det = db.Warehouses.FirstOrDefault(x => x.Id == headphone.WarehouseId);

            if(warehouse_det!= null)
            {
                headphone.Warehouse = warehouse_det;
            }

            db.Headphones.Add(headphone);//adding the sent headphone data to the table
            await db.SaveChangesAsync();


            //now, return a DTO object, intead of the context object to the confirmation view after addition
            var dto_headphone = new HeadphoneDTO()
            {
                Id = headphone.Id,
                Name = headphone.Name,
                WarehouseName = headphone.Warehouse.Warehouse_Name  //okay, at this step we are raising a object null exception. thats because warehouse is null
            };

            //returning the dto object for the view.
            return CreatedAtRoute("DefaultApi", new { id = headphone.Id }, dto_headphone);
        }

        // DELETE: api/Headphones/5
        [ResponseType(typeof(Headphone))]
        public async Task<IHttpActionResult> DeleteHeadphone(int id)
        {
            Headphone headphone = await db.Headphones.FindAsync(id);
            if (headphone == null)
            {
                return NotFound();
            }

            db.Headphones.Remove(headphone);
            await db.SaveChangesAsync();

            return Ok(headphone);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HeadphoneExists(int id)
        {
            return db.Headphones.Count(e => e.Id == id) > 0;
        }
    }
}