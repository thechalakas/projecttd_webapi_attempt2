﻿using System.Web;
using System.Web.Mvc;

namespace ProjectTD_WebAPI_attempt2
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
