﻿// this is the viewmodel. It's called ViewModel
//alright, this is the first time I am using knockout js. 
//luckily for me, intellisense is doing the usual typing helping stuff
var ViewModel = function ()
{
    var self = this;

    self.headphones = ko.observableArray(); //array for headphones.
    self.error = ko.observableArray(); //single element that gets updated when errors happen, 
    self.detail = ko.observable(); //single element that keeps track of details
    self.warehouses = ko.observableArray(); //array for warehouses
    self.newheadphone = 
        {
            Name: ko.observable(),
            Category: ko.observable(),
            Price: ko.observable(),
            Warehouse: ko.observable()
        }  //single elment that will be collected from the entry form

    //I guess. I am doing a lot of guessing because, I am simply following the
    //tutorial here. I will learn knockout js later. I have to. I dont have a choice.

    var headphonesUri = '/api/Headphones/';  //this is the headphones api link

    function ajaxHelper(uri, method, data)
    {
        self.error(''); //this will clear all the error messages. I suppose this is required to erase earlier messages.

        return $.ajax(
            {
                type: method,
                url: uri,
                dataType: 'json',
                contentType: 'application/json',
                data: data ? JSON.stringify(data) : null
            }//end of the ajax function stuff
            ).fail(
                function (jqXHR, textStatus, errorThrown)
                {
                    self.error(errorThrown);  //I guess this throws the error
                }//end of the errorThrown fail function
            );//end bracket of the fail function
    }//end of function ajaxHelper

    //function that will return all the headphones by calling the API
    function getAllHeadphones()
    {
        ajaxHelper(headphonesUri, 'GET').done(
            function (data)
            {
                self.headphones(data);
            }//end of function data
            );//end of done stuff
    }//end of all get all headphones

    //this method will collect the headphone detail
    self.getheadphonedetail = function (item)
    {
        ajaxHelper(headphonesUri + item.Id, 'GET').done(
                function (data)
                {
                    self.detail(data);
                }
            );
    }

    //get the headphone data to display
    getAllHeadphones();

    //now doing the warehouse stuff, and also adding a new headphone. 
    var warehouseUri = '/api/Warehouses/';

    //this will return all warehouses
    function getAllWarehouses()
    {
        ajaxHelper(warehouseUri, 'GET').done(
            function (data)
            {
                self.warehouses(data);
            }//end of function data that collects ware house data
            );//end of function done
    }//end of getAllWarehouses

    //this will add a new headphone
    self.addheadphone = function(formElement)
    {
        var headphone = {
            WarehouseId: self.newheadphone.Warehouse().Id, //collecting the ware house id
            Name: self.newheadphone.Name(),  //name of the headphone
            Price: self.newheadphone.Price(), //price of the headphone
            Category: self.newheadphone.Category() //category of the headphone.
        };//end of headphone object. 

        ajaxHelper(headphonesUri,'POST',headphone).done(
                function(item)
                {
                    self.headphones.push(item);
                }//end of function item
            );//end of the done function
    }//end of function formelement

    //get all warehouses
    getAllWarehouses();

};//end of ViewModel

//Does the data binding between the model and the view(model)
//apparently this uses the MVVM model for the UI design
ko.applyBindings(new ViewModel());
