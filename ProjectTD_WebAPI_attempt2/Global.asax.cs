﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ProjectTD_WebAPI_attempt2
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //okay, this code here is interesting.
            //You see, right now, the JSON responses come back as something like this.

            /*
             * <string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">{"Id":5,"Name":"Skull Candy 3","Category":"Wireless","Price":500.00,"WarehouseName":"Mysore"}</string>
             * 
             */

            //see, we dont that that string nonsense. we want a pure string. that's xml still butting around. I need to get rid of that. 
            //That is the xml formatter in action. So, I am removed that from the formatting. 
            var formatters = GlobalConfiguration.Configuration.Formatters;
            formatters.Remove(formatters.XmlFormatter);
        }
    }
}
