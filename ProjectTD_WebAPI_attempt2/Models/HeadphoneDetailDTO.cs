﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectTD_WebAPI_attempt2.Models
{
    //this is similar to HeadphoneDTO class but it has a little more detail
    public class HeadphoneDetailDTO
    {
        //this is the primary key stuff. 
        public int Id { get; set; }
        //this is the headphones name. right now there are no limitations.
        public string Name { get; set; }
        //category like, wireless or not wireless
        public string Category { get; set; }
        //price stuff. Like 100.10 rupees. 
        public decimal Price { get; set; }

        //this will hold the warehouse name. 
        //you will notice that in the Headphone model this is an actual object of type Warehouse. 
        //here it is essentially a simple string that will hold the name of the warehouse.
        public string WarehouseName { get; set; }
    }
}