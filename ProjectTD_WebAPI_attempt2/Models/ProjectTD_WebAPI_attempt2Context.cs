﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjectTD_WebAPI_attempt2.Models
{
    public class ProjectTD_WebAPI_attempt2Context : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public ProjectTD_WebAPI_attempt2Context() : base("name=ProjectTD_WebAPI_attempt2Context")
        {
            //this particular comment will allow me trace the db operations that EF executes. Very useful while building the app. 
            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        public System.Data.Entity.DbSet<ProjectTD_WebAPI_attempt2.Models.Warehouse> Warehouses { get; set; }

        public System.Data.Entity.DbSet<ProjectTD_WebAPI_attempt2.Models.Headphone> Headphones { get; set; }
    }
}
