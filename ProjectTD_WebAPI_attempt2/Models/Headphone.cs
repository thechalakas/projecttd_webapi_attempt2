﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectTD_WebAPI_attempt2.Models
{
    public class Headphone
    {
        //this is the primary key stuff. 
        public int Id { get; set; }
        //this is the headphones name. right now there are no limitations.
        public string Name { get; set; }
        //category like, wireless or not wireless
        public string Category { get; set; }
        //price stuff. Like 100.10 rupees. 
        public decimal Price { get; set; }

        //till now it was all headphone stuff
        //now we need to establish a connection from this headphone to the warehouse where it is stored

        //for that we use a foreign key
        public int WarehouseId { get; set; }

        //this is the navigation property man. 
        public Warehouse Warehouse { get; set; }
    }

}
