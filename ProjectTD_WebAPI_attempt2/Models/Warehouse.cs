﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectTD_WebAPI_attempt2.Models
{
    public class Warehouse
    {
        //need a int property, obviously.
        public int Id { get; set; }

        //stores the name of the warehouse
        [Required]
        public string Warehouse_Name { get; set; }
    }
}
