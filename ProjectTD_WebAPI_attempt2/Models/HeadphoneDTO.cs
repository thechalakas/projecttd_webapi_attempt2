﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectTD_WebAPI_attempt2.Models
{
    //this is DTO (Data Transfer Object) class for the Headphone model
    //it only contains limited details
    public class HeadphoneDTO
    {
        //this is the primary key stuff. 
        public int Id { get; set; }
        //this is the headphones name. right now there are no limitations.
        public string Name { get; set; }
        //category like, wireless or not wireless

        //this will hold the warehouse name. 
        //you will notice that in the Headphone model this is an actual object of type Warehouse. 
        //here it is essentially a simple string that will hold the name of the warehouse.
        public string WarehouseName { get; set; }
    }
}