# README #

Alright, this is the repo that demos a simple web api using c sharp / dot net . This is a continuation of the earlier solution available at 
https://bitbucket.org/thechalakas/projecttd_webapi_attempt1

This time, we are doing an online version of things. You know, with an online database, and with the files pushed on azure and stuff. 

### MOVED TO GITHUB ###

I have stopped using bitbucket. This project - still functional - has been moved to the following github location

https://github.com/Jay-study-nildana/ProjectTDWebAPI

all future updates will be available at that location.

### CORS Support Added ###

Since browsers started enforcing CORS like crazy, this API stopped working for javascript based web browser based JSON API consumption. Postman, android and iOS apps were unaffected. 

Now, I have added that supported, so that is no longer a issue. 

Specifically you wont get errors like this - Cross-Origin Request Blocked: The Same Origin Policy disallows reading the remote resource

### deployments ###

It is currently deployed at the following site 

http://simplewebapi1webapp1.azurewebsites.net

It has a basic - but functional - API document that allows all the four things that you would do while practising a API consuming project. 
CRUD. Good luck. 

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 